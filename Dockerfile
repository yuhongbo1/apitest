# Use an official Python runtime as a parent image
FROM python:3.8-slim-buster

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Create virtual environment and install the application dependencies
RUN python -m venv venv
RUN /bin/bash -c "source venv/bin/activate && pip install --upgrade pip && pip install -r requirements.txt"

# Make port 5000 available to the world outside this container
EXPOSE 8000

# Define environment variable
ENV FLASK_APP="app.py"

# Run app.py using the virtual environment
CMD ["/bin/bash", "-c", "source venv/bin/activate && flask run -p 8000 -h 0.0.0.0"]
